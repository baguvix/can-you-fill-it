/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pcorlys- <pcorlys-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/24 18:13:36 by pcorlys-          #+#    #+#             */
/*   Updated: 2019/02/19 20:10:00 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		main(int argc, char **argv)
{
	int				count_figurs;
	unsigned short	root;
	t_crd			*head;
	t_crd			*copy;

	count_figurs = main_check(argc, argv);
	head = read_figurs(argv);
	root = root_area(count_figurs, head) - 1;
	copy = NULL;
	// твоя функция ниже.
	// its necessary to initialize copy by NULL at first, and if we looking for full covering on square with side equal to 4, 
	// then u must pass the root argument as 3, cause 0,1,2,3, hope you got it))
	// P.S. tried to write my first comment at Eng, dont be to rigor to me c:
	// p.p.s. check ur create_area algorithm, look at the result with 2 figures, that fit at 3width side square and run it with 4. I bet u didn`t know))
	while (copy == NULL)
	{
		copy = fill_it(head, copy, root);
		root++;
	}
	create_area(copy, root);
	// lst free
}
