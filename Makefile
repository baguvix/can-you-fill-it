# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/02/19 19:39:04 by orhaegar          #+#    #+#              #
#    Updated: 2019/02/19 20:21:00 by orhaegar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
NAME = fillit
HEADSL = libft/
HEADS = fillit.h libft.h
CFLAGS = -Wall -Werror -Wextra -I$(HEADSL)
SRCS = checker.c		\
		create_area.c	\
		error.c			\
		fill_it.c		\
		free_lst.c		\
		list_figurs.c	\
		main.c			\
		root.c

OBJS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(OBJS)
	make -C $(HEADSL) 
	$(CC) $(CFLAGS) -L$(HEADSL) -lft $(OBJS) -o $@

%.o: %.c $(HEADS)

clean:
	rm -f $(OBJS)
	make clean -C $(HEADSL)

fclean: clean
	rm -f $(NAME)
	make fclean -C $(HEADSL)
re: fclean all
